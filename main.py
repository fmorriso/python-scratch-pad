"""a little python scratch pad"""
import re
import sys

from pizza import Pizza
from data_transfer_object import DTO

global_number = 7

def get_python_version() -> str:
    """ the version of python running this program"""
    return f'{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}'


def make_2d_grid() -> list[list[int]]:
    grid: list[list[int]] = []
    square_size: int = 8
    single_row = [0] * square_size
    for i in range(square_size):
        grid.append(single_row.copy())

    return grid


def print_grid(grid: list[list[int]]) -> None:
    """display the 2d square grid """
    for i in range(len(grid)):
        print(" ".join([str(cell) for cell in grid[i]]))


def get_ready_for_checkers(grid: list[list[int]]) -> None:
    for i in range(len(list2d)):
        for j in range(len(list2d[i])):
            if i < 3 or i > 4:
                list2d[i][j] = 1


def first_char_last_name(name: str) -> str:
    pieces = name.split()
    last = pieces[-1]
    just_first = last[0]
    return just_first


def code_hs_last_initials():
    # 9.2.8a Last Initials
    names = [
        "Maya Angelou",
        "Chimamanda Ngozi Adichie",
        "Tobias Wolff",
        "Sherman Alexie",
        "Aziz Ansari"
    ]
    print([first_char_last_name(name) for name in names])
    print([name.split()[-1][0] for name in names])


# import re
def remove_comments(text):
    """https://www.codeease.net/programming/python/python-remove-all-comments"""
    pattern = r"(\".*?\"|\'.*?\')|(/\*.*?\*/|#[^\r\n]*$)"
    regex = re.compile(pattern, re.MULTILINE | re.DOTALL)
    return regex.sub("", text)


def test_remove_comments():
    code = """
    # This is a comment
    print("Hello, World!") # This is another comment
    """
    code += '\n"""docstring comment"""'
    print(f'before: {code=}')
    # Remove comments
    code_without_comments = remove_comments(code)
    # Output
    print(f'after: {code_without_comments=}')


def verify_pizza_class():
    p = Pizza(['cheese', 'tomatoes'])
    print(f'{p=}')
    # FAILS: print(f'{Pizza.margherita()=}')


def question():
    """
    Quizz question
    """
    accumulator = 0
    for x in range(5):
        if x > 2:
            accumulator += 1
    else:
        accumulator += 1

    print(accumulator)


def update_counts(count_dictionary, word):
    """CodeHS 9.5.7a Word Lengths, Part 2"""
    if word is not type(str):
        word = str(word)
    if word in count_dictionary:
        count_dictionary[word] = count_dictionary[word] + 1
    else:
        count_dictionary[word] = 1


def verify_update_counts():
    my_dictionary = {}

    words = input("a phrase: ")
    my_list = list(words)

    for word in my_list:
        update_counts(my_dictionary, word)

    print(f'{my_dictionary=}')


def verify_while_with_break_and_else():
    a: int = 0
    while a < 5:
        print(f'{a=}')
        a += 1
        if a == 3:
            print('about to break')
            break
    else:
        print('else')
    print('after while')


def q421():
    number = 6
    value = number
    while number != 0:
        value *= -1
        number -= 1

    if value > 0:
        b = False
    else:
        b = True
    print(b, value, number)

def show_global_vs_local() -> None:
    global_number = 1
    print(f'inside method that shadowed it, {global_number=}')

def pass_in_global_variable(n:int) -> int:
    print(f'inside method, {n=}')
    n +=  2
    return n

def modify_global_in_list(lst: list[int]) -> None:
    print(f'inside method, before changing it, {lst=}')
    lst[0] = 9
    print(f'inside method, after changing it, {lst=}')

def manipulate_global_variable_using_object() -> None:
    payload = DTO(global_number)
    print(f'before changing, {payload.number=:.2f}')
    payload.number = 6.5
    print(f'after changing, {payload.number=:.2f}')


if __name__ == '__main__':
    print(f'Python version {get_python_version()}')

    manipulate_global_variable_using_object()

    # print(f'before shadowing it, {global_number=}')
    # show_global_vs_local()
    # print(f'after leaving method that shadowed it, {global_number=}')
    #
    # pass_in_global_variable(pass_in_global_variable(global_number))
    # print(f'after leaving method that changed it, {global_number=}')
    #
    # global_in_list: list[int] = [global_number]
    # print(f'before calling method with list, {global_in_list=}')
    # modify_global_in_list(global_in_list)
    # print(f'after leaving method that changed it, {global_in_list=}')
    # global_number = global_in_list[0]
    # print(f'after leaving method that changed it, {global_number=}')
    #

    #q421()
    # list2d = make_2d_grid()
    # print(f'{list2d=}')
    # print_grid(list2d)
    # print('-' * 50)
    # get_ready_for_checkers(list2d)
    # print_grid(list2d)
    # code_hs_last_initials()
    # test_remove_comments()
    # verify_pizza_class()
    # question()
    # verify_update_counts()
    # verify_while_with_break_and_else()
